<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<div class="colL">
    <h1>The Objectives of our Programmes</h1>
	<ul>
		<li>To teach children to verbalise their feelings in different social situations</li>
		<li>To teach children to make positive life choices in different situations</li>
		<li>To teach children a strong sense of discipline and respect for others </li>
		<li>To increase children's self esteem and resilience</li>
		<li>To improve children's safety knowledge and skills</li>
		<li>To increase children's sense of body pride</li>
		<li>To improve children's ability to disclose inappropriate behaviour and sexual abuse</li>
		<li>To improve children's empathy towards others</li>
     </ul>
     </div><!-- colL end -->

     <div class="colR values">
     	<h1>Our Values</h1>
     	<p><h2>RESPECT</h2>We show honour and consideration and we esteem every person in view of our diversity.</p>
		<p><h2>HOPE</h2>We are committed to faith and hope in God, through Christ Jesus, and we  work towards restoration and bring hope for a better future in our society.</p>
		<p><h2>CHOICES</h2>We believe that making good choices is key to building a healthy nation. In this light, we choose to deliver quality services and products, to be accountable, and to exercise best practice in all that we do.</p>
		<p><h2>SELF-WORTH</h2>We believe every person is created in God's image and we strive to empower everyone we come into contact with to believe this about themselves.</p>
	</div><!-- colR end -->

	<div class="dash"></div>

	<h1>Our Strategy</h1>
    <p style="text-align:center;"><img src="images/Strategy.png" width="471" height="490" /></p>


</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
