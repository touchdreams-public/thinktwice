<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>

  	<div class="contentContainer">

    	<iframe src="https://www.wcea.education/files/assets/html/wceaWidget.html?ai=302" id="bundlesIframe"
				scrolling="no" style="
					border:none;
					width: 1px;
					min-width: 100%;
					height: 2150px;
					overflow: hidden;"></iframe>
			<!-- jQuery to enable the widget to resize correctly -->
			<script src="https://www.wcea.education/files/assets/js/resizer.js"></script>
			<script src="https://static.hsappstatic.net/jquery-libs/static-1.48/jquery/jquery-2.1.3.js"></script>
			<script type="text/javascript">
				$(window).on('load',function(){iFrameResize({ log: true }, '#bundlesIframe');});
			</script>

    </div>
    <?php include_once('partials/footer.php'); ?>
  </div>
</body>
</html>
