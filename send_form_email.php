<?php
if(isset($_POST['email'])) {
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "enquiries@thinktwice.co.za";
    $email_subject = "Think Twice (Projects page)";
     
     
    function pageRedirect ($page) {
    if (!@header("Location: ".$page))
       echo "\n<script type=\"text/javascript\">window.location.replace('$page');</script>\n";
    exit;
}

        // your error code can go here
        print "<meta http-equiv=\"refresh\" content=\"0;URL=http://thinktwice.org.za/programmesoops.php#form\">";
    }
     
    // validation expected data exists
    if(!isset($_POST['first_name']) ||
        !isset($_POST['last_name']) ||
		!isset($_POST['cell']) ||
		!isset($_POST['telephone']) ||
		!isset($_POST['city']) ||
		!isset($_POST['province']) ||
        !isset($_POST['email'])) {
        
		      
    }
     
    $first_name = $_POST['first_name']; // required
    $last_name = $_POST['last_name']; // required
	$cell = $_POST['cell']; // required
    $telephone = $_POST['telephone']; // required
	$city = $_POST['city']; // required
    $province = $_POST['province']; // required
	$email_from = $_POST['email']; // required


     
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
    $string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(!preg_match($string_exp,$last_name)) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }
  	$string_exp = "/^[0-9.'-]+$/";
    if(!preg_match($string_exp,$cell)) {
    $error_message .= 'The Cell number you entered does not appear to be valid.<br />';
  }
  $string_exp = "/^[0-9.'-]+$/";
    if(!preg_match($string_exp,$telephone)) {
    $error_message .= 'The Daytime Telephone number you entered does not appear to be valid.<br />';
  }
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "Form details below.\n\n";
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
     
    $email_message .= "First Name: ".clean_string($first_name)."\n";
    $email_message .= "Last Name: ".clean_string($last_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Cell: ".clean_string($cell)."\n";
    $email_message .= "Daytime Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Province: ".clean_string($province)."\n";
	$email_message .= "City: ".clean_string($city)."\n";
     
     
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
if (!@header("Location: ".$page))
       print "<meta http-equiv=\"refresh\" content=\"0;URL=http://thinktwice.org.za/programmesreply.php#form\">";
    exit;
?>
 


<?php
if(isset($error))
    echo "<span id='Warning'>Please enter all areas marked with *</span>";
else 
    pageRedirect ($programmesreply);?>