<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>National Child Protection Week</h1>
  <p>National Child Protection Week (CPW) is observed in South Africa annually at the end of the month of May to raise awareness of the rights of children as articulated in the Children's Act of 2005. However, to us Child Protection is a daily responsibility. According to the Department of Social Development, 1 in 3 children in South Africa experience a form of child abuse. For this reason, we're calling on you to help protect our children everyday.</p>
	<p>As ThinkTwice, we envision a world where children have strong foundation in their lives for healthy sexuality. This means:</p>
	<ul>
		<li>That they have a voice.</li>
		<li>That they are free to express themselves</li>
		<li>To have the courage to speak up against any injustice towards them.</li>
		<li>Protected from abuse and neglect.</li>
		<li>And empowered to grow to their full potential</li>
	</ul>
	<p>A South Africa with zero child abuse is possible. Let’s make it happen</p>
	<hr>
	<p>Click on the frames below to download them and show your support!</p>
	<p>
		<a download href="images/NCPW-yellow.png"><img style="width: 280px;" src="images/NCPW-yellow-500.png" /></a>
		<a download href="images/NCPW-green.png"><img style="width: 280px;" src="images/NCPW-green-500.png" /></a>
	</p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
