<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Testimonials: Trainees</h1>
    <img src="images/testimonials-trainees01.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px;" />
	<p>A mother first and teacher second have I learned a lot about how to communicate with children and how to take care of children.<br /> <strong>- Grace educare</strong></p>
    <p>My impression of the workshop is that that its absolutely amazing. It kept us entertained while teaching us a lot. The layout of the presentation was good. <br /> <strong>- Arthur -Jenny's educare</strong></p>
    <p>We need this in our churches and communities because we need the skills, tools and techniques to handle what's happening to our children. <br /><strong>- Nomhle</strong></p>
    <p>It's for real the topics we addressed because we experience this situations in our classrooms, Thus bringing the solutions that think twice brings close to home and real to us. <br /><strong>- Disa Primary</strong></p>
    <p>You are never too old to learn. It was very educational and gave me more confidence in my job. I would recommend that everyone teacher and Parent to get trained in it. <br /><strong>- Wavecrest Nursery school</strong> </p>
    <p>Excellent workshop! It was helpful and understandable. <br /><strong>- Springwood</strong></p>
    <p>It was very interesting I wish it could go to all the communities.<br /> <strong>- Home from home</strong></p>
    <p>Good idea with the icebreakers I enjoyed shaking loose. I found it very informative and educational. <br /><strong>- Habibia Junior</strong></p>

</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
