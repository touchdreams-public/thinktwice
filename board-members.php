<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Board Members</h1>

    <p>The Board of Directors is a team of volunteers dedicated to supporting ThinkTwice in its community service endeavours. In general the board operates in the following way:
		<ul>
        	<li>As the trustees of the organisation</li>
        	<li>Ensure that responsible staff is put in place to aid ThinkTwice discharge its duties to the communities as promised in its mission and values statements.</li>
        	<li>Ensure that ThinkTwice acts within the laws governing the operations of non-profit organisations i.e. develop policies</li>
        	<li>They help create ThinkTwice's vision, mission and value statements</li>
        	<li>They help with strategic plan to provide ThinkTwice with resources to aid accomplish its mission</li>
        	<li>Support, coach, mentor staff where applicable</li>
       </ul>
	</p>
    <p>If you are interested in becoming a board member of ThinkTwice, please contact us.</p>

</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
