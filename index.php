<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>

		<section class="banner">
			<p><a href="/ncpw.php">It's Child Protection Day every day.</p>
		</section>

		<section class="banner secondary">
			<p><a href="/courses.php">Browse our partner's online courses</a><br>
			<small>High quality accredited courses in healthcare, business, etc.</small></p>
		</section>

    <div class="homeSlider">
	    <div id="wrapper">
		  	<div class="slider-wrapper theme-default">
					<div class="ribbon"></div>
	        <div id="slider" class="carousel"><!-- nivoSlider -->
            <img src="images/img03.jpg" alt="" title="" data-transition="fold" />
            <!-- <img src="images/img04.jpg" alt="" title="" data-transition="slideInLeft" /> -->
            <!-- <img src="images/img06.jpg" alt="" title="" data-transition="boxRandom" /> -->
						<img src="images/home/welcome1_3x1.jpg" /></a>
						<img src="images/home/welcome2_3x1.jpg" /></a>
						<img src="images/home/welcome3_3x1.jpg" /></a>
						<img src="images/home/welcome4_3x1.jpg" /></a>
	        </div>
	      </div>
	    </div>
    </div>

		<div class="contentContainer">

			<h1 style="font-size: 3.4em; text-align:center;">Vision</h1>
			<p class="vision">Children have a strong foundation in their lives for a healthy sexuality.</p>

			<h2 class="h1" style="font-size: 3.4em; text-align:center;">Mission</h2>
			<p>We collaborate with the relevant stakeholders to train, equip and empower educators, caregivers and community workers at large with information, tools and resources to provide the children in their care with the awareness, knowledge and skills required for their safety and healthy sexuality.</p>

			<!-- <div class="dash"></div> -->

			<div class="picsHome" style="text-align:right; width: 237px;">
				<a href="video.php"><img src="images/jerry.png" alt="Click here to watch our video" width="237" height="511" border="0"/></a><br />
			</div>

			<div class="contactBlock">
	    		<h2 class="h1">Please contact us</h2>
				<p class="contactPerson">
					<strong>CONTACT PERSON:</strong><br />
					Marcia Sibara<br /><em>Executive Director</em>
				</p>
	      		<p>
	      			<!-- <span class="contactNumbers"><strong>T:</strong> 021 689 8331</span> -->
	        		<!-- <span class="contactNumbers"><strong>C:</strong> 081 387 9895</span> -->
	      			<span class="contactNumbers"><strong>Moussa:</strong> <a href="tel:+27835837481">083 583 7481</a></span>
	        		<span class="contactNumbers"><strong>Lana:</strong> <a href="tel:+27744196331">074 419 6331</a></span>
	        	</p>
				<!-- <span class="contactNumbers"><strong>F:</strong> 086 607 9482 </span> -->
	      		<p>
	      			<span class="contactLinks"><strong>E:</strong> <a href="mailto:info@thinktwice.org.za">info@thinktwice.org.za</a></span>
					<span class="contactLinks">Follow us on:
						<a href="https://www.facebook.com/pages/ThinkTwice" target="_blank">Facebook</a></span>
				</p>

	      		<p>
					<strong style="color:#ed6d2c;">PHYSICAL / POSTAL ADDRESS:</strong><br />
	        		14 Park Road, Rondebosch 7700<br />
	        		Cape Town, Western Cape, South Africa<br />
					<b>AND</b><br />
					Limpopo branch<br />
					Stand No. 704 Tshilivho,<br />
					Makhuvha Village<br />
					Thohoyandou<br />
					0950
				</p>

				<div class="ngo" style="float: none; margin-left: 15px; overflow: auto;">
					<p>
						<strong style="color:#ed6d2c;">Our registration details are as follows: </strong><br />
						<span>Non-Profit Organisation Registration number:</span> 031-678 NPO<br />
						<span>Public Benefit Organisation (PBO) Reference number:</span> 930011769<br / />
						<span>ThinkTwice ETDP registration number: ETDP10873</span>
					</p>

					<!-- <img src="images/logos/etdp-seta-logo.jpg" style="max-width: 100px; float: left;" /> -->
				</div>

	      		<p><a href="strategic-partners.php#donate" class="bttn-donate"></a></p>
			</div><!-- contact end -->
		</div>
	<?php include_once('partials/footer.php'); ?>
	</div>
</body>
</html>
