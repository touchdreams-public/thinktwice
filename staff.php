<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Staff</h1>

	<!-- Lana.jpg -->
	<!-- Marcia.jpg -->
	<!-- Moussa.jpg -->
	<!-- Mwamine.jpg -->

	<!-- <div class="team">
    <img src="images/team/Marcia.jpg" alt="Profile Picture" class="profile" />
		<h2>Marcia Sibara</h2>
    <p><em>Executive Director</em></p>
	</div> -->

	<!-- <div class="team"> -->
    <!-- <img src="images/moussa.png" alt="Profile Picture" class="profile" /> -->
    <!-- <img src="images/team/Moussa.jpg" alt="Profile Picture" class="profile" /> -->
		<!-- <h2>Moussa K. Mulamba</h2> -->
    <!-- <p><em>Senior Manager</em></p> -->
		<!-- <p>Moussa is a qualified nurse and worked as a primary health practitioner for almost 2 years. He underwent HIV  &amp; AIDS training in peer education and sexual and reproductive health issues and had shared the knowledge at various local and international conferences. He also holds a Masters degree in International Development from Eastern University (www.eastern.edu), has experience in pastoral ministry as well as experience in interpretation (French-English). As a husband and father, Moussa promotes and supports women development, their respect and their values. He is passionate about the welfare of children.</p>
		<p>Moussa worked for ThinkTwice as a volunteer in 2003. He joined the staff in February 2005 and has served ThinkTwice as the Delivery Projects Manager until December 2013. His primary role was to ensure that all ThinkTwice programmes are managed efficiently and professionally. As an asset Moussa has developed and acquired, among many others, the following skills: projects management, strategic planning, administration, advertising and marketing, teaching, trainer/mentor and facilitation/presentation, public speaking skills, debating and motivational speaker. Moussa understands the needs for holistic development and is passionate about it happening. In his new capacity as the Executive Director, Moussa looks forward to assisting ThinkTwice in developing plan for self-sustainability. He is passionate about the work ThinkTwice does and would like to see it continue as long as possible.</p>
		<p class="nicknames">His colleagues say that he is "Mr. Smooth".</p> -->
		<!-- <p><strong>Contact Moussa directly at:</strong> <a href="mailto:info@thinktwice.org.za">info@thinktwice.org.za</a> or <a href="mailto:moussa@thinktwice.org.za">moussa@thinktwice.org.za</a></p> -->
	<!-- </div> -->

	<!-- <div class="team">
    <img src="images/team/Mwamine.jpg" alt="Profile Picture" class="profile" />
		<h2>Mwamine R. Saffi</h2>
    <p><em>Projects Coordinator</em></p>
	</div> -->

	<!-- <div class="team">
    <img src="images/team/Lana.jpg" alt="Profile Picture" class="profile" />
		<h2>Lana Hendrikse</h2>
    <p><em>Administrator</em></p>
	</div> -->


	<div class="cols">
		<div>
			<img src="images/team/Marcia.jpg" />
			<h3>Marcia Sibara</h3>
			<p>Executive Director</p>
		</div>
		<div>
			<img src="images/team/Moussa.jpg" />
			<h3>Moussa K. Mulamba</h3>
			<p>Senior Manager</p>
		</div>
		<div>
			<img src="images/team/Mwamine.jpg" />
			<h3>Mwamine R. Saffi</h3>
			<p>Projects Coordinator</p>
		</div>
		<div>
			<img src="images/team/Lana.jpg" />
			<h3>Lana Hendrikse</h3>
			<p>Administrator</p>
		</div>
	</div>

  <!-- <div class="team"> -->
		<!-- <img src="images/miemie.png" alt="Miemie Saffi" class="profile" /> -->
		<!-- <h2 style="margin-bottom:0;">MIEMIE SAFFI</h2> -->
    <!-- <p style="margin-top:0;"><em>Qualified Facilitator</em></p> -->
		<!-- <p>Miemie is a qualified facilitator and has experience facilitating ThinkTwice programmes since March 2008 and enjoys working with teachers, learners and children's caregivers.
  	<p>Miemie strongly believes in two things; one of them is that it takes a village to raise a child and the second is that children are a treasure and the future. If we strongly believe in the future, then it takes you and me to be the change we want to see. </p>
		<p>She is passionate about restoring that which is broken and stolen from the little one's upbringing and identity.</p>
  	<p>Miemie is an invaluable catalyst within (the glue that holds the) the delivery department (together)!</p>
		<p class="nicknames">Her colleagues describe her as "Speedy Gonzalez".</p> -->
		<!-- <p><strong>You can contact Miemie directly at:</strong> <a href="mailto:miemie@thinktwice.org.za">miemie@thinktwice.org.za</a></p> -->
	<!-- </div> -->

  <div style="clear:both;"></div>
  <!--<div class="team">
		<img src="images/deirdre.png" alt="Deirdre Solomon" class="profile" />
		<h2 style="margin-bottom:0;">DEIRDRE SOLOMON</h2>
        <p style="margin-top:0;"><em>Monitoring and Evaluation</em></p>
		<p>Deirdr� is concerned for children�s safety in an ever-increasingly violent and abusive South African society. If there is one thing that really affects her deeply, it is the sexual abuse and violation of girls and boys and the devastating effects this can have on them their entire lives.</p>
		<p>Deirdr� has a Bachelor of Business Science (Honours) Degree in Marketing from UCT. She has four years experience in advertising, marketing and research in the business environment as well as five years higher education experience in lecturing and programme management in Marketing and Marketing Research at UCT. She also has project management experience in the FET sector as well as 15 years bookkeeping experience.</p>
		<p>In 2013, in order to evaluate social programmes and their effectiveness in the social, political and economic arenas, Deirdr� pursued a Masters Degree in Programme Evaluation at UCT. Through her Masters research, specifically researching similar child sexual abuse prevention programmes worldwide as the ThinkTwice programmes, she became very interested in continuing in this field of research.</p>
		<p>Having served as the executive director for three and a half years until the end of 2013, Deirdr�  will now be overseeing monitoring and evaluation at ThinkTwice as well as pursuing a doctorate this year. Thus, whilst she is still committed to the work ThinkTwice does in the communities it serves, her own personal vision has expanded.</p>
		<p>Her goal is to undertake research which will evaluate current programmes and systems which are in place and to impact strategies in child health, education and our legal system. Her wish is to be able to contribute to civil society and governmental policies to bring about a drastic reduction of child sexual abuse in South Africa and the successful conviction of perpetrators.</p>
	  	<p class="nicknames">Her colleagues call her "mama bear".</p>
		<p><strong>Contact Deirdr� directly at</strong>: <a href="mailto:deirdre@thinktwice.org.za">deirdre@thinktwice.org.za</a></p>
	</div>-->


		<h2>Board Members</h2>

    <p>The Board of Directors is a team of volunteers dedicated to supporting ThinkTwice in its community service endeavours. In general the board operates in the following way:</p>
		<ul>
    	<li>As the trustees of the organisation</li>
    	<li>Ensure that responsible staff is put in place to aid ThinkTwice discharge its duties to the communities as promised in its mission and values statements.</li>
    	<li>Ensure that ThinkTwice acts within the laws governing the operations of non-profit organisations i.e. develop policies</li>
    	<li>They help create ThinkTwice's vision, mission and value statements</li>
    	<li>They help with strategic plan to provide ThinkTwice with resources to aid accomplish its mission</li>
    	<li>Support, coach, mentor staff where applicable</li>
		</ul>
		<div class="cols board-member-profiles">
			<div>
				<img src="images/staff/Pule.jpg" />
				<h3>Pule Keswa</h3>
				<p>Chairperson</p>
			</div>
			<div>
				<img src="images/staff/Takadzani.jpg" />
				<h3>Takadzani Mudau</h3>
				<p>Treasurer</p>
			</div>
			<div>
				<img src="images/staff/Joshua.jpg" />
				<h3>Joshua Tshidzumba</h3>
				<p>Secretary</p>
			</div>
			<div>
				<img src="images/staff/Lenny.jpg" />
				<h3>Lenny Tshilande</h3>
				<p>Additional member</p>
			</div>
			<div>
				<img src="images/staff/Mymoena.jpg" />
				<h3>Mymoena Anthony</h3>
				<p>Additional member</p>
			</div>
		</div>
    <p>If you are interested in becoming a board member of ThinkTwice, please contact us.</p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
