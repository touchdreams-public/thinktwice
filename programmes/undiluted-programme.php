<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Undiluted Programme</h1>
	<img src="../images/undiluted01.png" width="300" height="210" style="float: right; margin: 0 0 15px 0;" />
     <img src="../images/undiluted02.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px; clear:right;" />
    <p>The Undiluted programme was the initial vehicle ThinkTwice started using in the face of the growing challenge of teenage pregnancy in schools and HIV/AIDS as well as in the pursuit of youth empowerment. The Undiluted programme urges the youth to emulate sexually responsible choices and to live with a goal in mind of future aspirations. Today the programme is an ad hoc initiative! It is delivered on demand.</p>
    <p><strong>The Undiluted programme is designed for grade 8-10 learners. It is run during Life Orientation periods. However its message could be used wherever applicable to the above-referred age group. The Undiluted programme touches on the following topics:</strong>
		<ul>
        	<li>Who am I</li>
        	<li>Self-esteem</li>
        	<li>Self-discipline</li>
        	<li>Relationship</li>
        	<li>Psycho-social support </li>
        	<li>Living with a vision</li>
        </ul>
     </p>
    <!-- <div class="needsContact" style="clear:both;"><strong>If you are interested in booking the<br />Undiluted programme with ThinkTwice please contact:</strong><br /> Miemie Saffi at <a href="mailto:miemie@thinktwice.org.za">miemie@thinktwice.org.za</a> or 021 761 3338.</div> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
