<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>

    <div class="homeSlider">
		<!-- InstanceBeginEditable name="Image Slider" -->Image Slider
    </div>



<div class="contentContainer">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/programmes.png" alt="Programmes" width="175" height="46" /><br />
	<div class="crayonBox">
		We achieve our vision by training, equipping and resourcing educators and community workers who impact children with child-friendly programmes and vital information designed to help children build good self-esteem and to encourage them to achieve their dreams by making good decisions today.
	 </div>
	<p></p>
	<img src="../images/pics_programs2.png" alt="Program Photos" width="249" height="448" class="picsHome" style="margin-bottom: 30px;" />
	<h1>Early Childhood Development (ECD)</h1>
	<p><span class="thinktwice">ThinkTwice</span> programmes lay strong foundations with children from an early age. All programmes are piloted in a number of different socio-economic contexts. They are reviewed by educators and Department of Education Curriculum advisors and are aligned with the National Curriculum Statement.</p>
	<p>We have age-appropriate programmes for Grades R to 3, which include topics on:
	  <ol>
	    	<li>Family</li>
	    	<li>Making choices</li>
	    	<li>Feelings</li>
	    	<li>Body pride</li>
	    	<li>Child abuse</li>
	    	<li>HIV/AIDS</li>
	  </ol>
	</p>
	<div class="dash"></div>
	<h1>1. Training Workshops</h1>
	<p><span class="thinktwice">ThinkTwice</span> trains educators from schools, informal cr&egrave;ches, other non-profit organisations and faith-based organisations to use our programmes. During 2-day training workshops, trainees are given the fundamentals of the programmes. They receive an educator�s manual containing the complete programme, information on HIV/AIDS &amp; child sexual abuse, referral lists, as well as a CD containing all songs used in the programme. Follow up and support are arranged accordingly.</p>
	<div class="dash"></div>
	<h1>2. Follow-up strategy</h1>
	<p>Trainees receive follow-up and support 4-6 weeks after training.</p>
	<p><span class="thinktwice">ThinkTwice</span> teams follow-up on trainees in their respective informal cr�ches in under-resourced areas to implement the programmes by going to the cr�che 2 to 3 times per week and, together with the teacher, facilitating the programme with the children in a class. As the weeks progress, the teacher completes most of the preparation and actual lesson in the class. At the end of the 15 lessons, teachers are able to run the programme successfully on their own with other groups of children. </p>

	<a name="form"></a><form><strong style="color:#FFFFFF;">Thank you. Your request has been sent!<br />Someone from our team will be in contact with you shortly.</strong></form>

</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
