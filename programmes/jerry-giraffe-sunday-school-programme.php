<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Jerry Giraffe Sunday School Programme</h1>
	<img src="../images/jerryss-01.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px;" />
	<p>The Jerry Giraffe Sunday School programme is made up of 10 lessons which teach young children (4 - 6 year old) the following important concepts:
    	<ul>
        	<li>family and relationships</li>
            <li>making positive choices</li>
            <li>encouraging good self esteem</li>
            <li>expressing feelings</li>
            <li>HIV/AIDS</li>
            <li>body pride and ownership</li>
            <li>child abuse awareness and safety skills</li>
            <li>modelling positive endings and saying goodbye</li>
        </ul>
    </p>

    <p>The lessons are all interactive and children learn through various methods such as bible stories, participation, questions and answers, songs, games, chants, icebreakers, reading stories and role-play.</p>
    <img src="../images/jerryss-02.png" width="300" height="210" style="float: left; margin: 0 15px 5px 0;" />
    <p>The Jerry Giraffe Sunday School programme is suitable for use in all Christian denominations and settings in order to impact children. Using lifeskills lessons we bring out God's heart for children. We have designed this programme to encourage churches to be aware of abuse that children might suffer if not protected at the hands of perpetrators. Child [sexual] abuse might happen wherever children are found. During 2011 ThinkTwice worked in collaboration with Fikelela AIDS Project to adapt the Jerry Giraffe programme for use in a Sunday school context. Fikelela means "reach out" and is the name of the HIV/AIDS outreach programme of the Anglican Church in Cape Town, South Africa.</p>
    <p>It is a resource we recommend to all Sunday school educators, church holiday clubs, children's camps, etc.</p>

		<!-- <div class="needsContact"><strong>For more info please contact:</strong> Miemie Saffi at <a href="mailto:miemie@thinktwice.org.za">miemie@thinktwice.org.za</a></div> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
