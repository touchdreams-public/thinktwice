<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Teacher Training Programme</h1>

    <img src="../images/teacher-01.png" width="300" height="210" style="float: right; margin: 0 0 10px 10px;" />
    <img src="../images/teacher-02.png" width="300" height="210" style="float: right;  clear: right; margin: 0 0 10px 10px;" />
	<p>The Teacher Training programme is aimed at cr&egrave;che teachers and Sunday School teachers to become effective facilitators of the Jerry Giraffe programmes. Teachers are trained to deliver the programmes to the children in their classes.  The training workshops cover the fundamentals of the programmes. These fundamentals include the use of specific tools and techniques such as the Jerry Giraffe persona doll and talking flower, circle time, songs, chants and role-play techniques. These all help maintain discipline, enhance learning and increase participation of learners during classroom activities. </p>
    <p>The training workshops also cover child sexual abuse dynamics and how to handle disclosure.  Programme facilitators are taught how to speak to children in age-appropriate ways about sexuality issues, including HIV/AIDS.</p>
    <p>As part of the training workshop, trainees receive the teachers' manual, storybook, and a cd with songs, chants and icebreakers used in the programme. The teacher's manual contains other useful resources, including sample letters addressed to parents which aid to involve parents in the programme and inform parents of the lessons their children will be learning. Some of the resources needed for delivery of the programme to the children the teachers are expected to make themselves as guided in the teacher manual. However, ThinkTwice provides the Jerry Giraffe persona doll with his talking flower for sale to workshop participants.</p>

		<!-- <div class="needsContact"><strong>To book for a workshop or to buy programme resources, please contact:</strong><br /> Miemie Saffi at <a href="mailto:miemie@thinktwice.org.za">miemie@thinktwice.org.za</a> or 021 761 3338. </div> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
