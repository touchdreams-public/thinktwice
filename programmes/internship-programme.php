<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Internship Programme</h1>
	<img src="../images/internship-01.png" width="300" height="210" style="float: right; margin: 0 0 15px 15px;" />
	<p>Research has shown that the current state of primary education in<strong> South Africa</strong> fails to provide our country's youngest students with the knowledge and life skills they need to grow and prosper in an <strong>increasingly complex world</strong>. The structural inequalities imbedded in South Africa during the Apartheid era and a lack of political will are frequently blamed for these problems, and rightly so. </p>
    <p>Through an internship with ThinkTwice or one of our partner agencies, however, you will meet inspiring teachers who do their best to provide high quality education to their students despite the challenges. You will <strong>befriend children</strong> from all walks of life, facing seemingly insurmountable challenges, and yet... daily they will greet you with heart-melting smiles desiring nothing more than another push on the swing set or to read another story from their favorite books.</p>

    <p class="quote">"All of the opportunities afforded to an intern to get out of the office and <strong>interact</strong> with the Cape Town community are what make an internship with ThinkTwice truly <strong>rewarding</strong>. Whether we are drafting a proposal for a <strong>health education</strong> event or helping to deliver ThinkTwice's programme in local schools, this experience is truly fulfilling. <em><strong>- Austin C. Lindsay</strong></em>"</p>

    <p>The children ThinkTwice and its partner organisations work with need people with hearts of gold that are prepared to help them, if only in a small way, transcend the seemingly insurmountable challenges and work on addressing the <strong>big issues</strong> they will face throughout their lives in Cape Town and beyond.
    <p>By doing an internship through ThinkTwice you are supporting ThinkTwice's goal of reaching children aged 4-6 years old with <strong>hope and empowerment </strong>through our lifeskills programmes.</p>
	<p>If you are passionate about <strong>investing</strong> in the lives of children and are looking to complete an internship in Cape Town, we would love to hear from you! We invite you to think about doing an <strong>internship</strong> with ThinkTwice. </p>
	<img src="../images/internship-02.png" width="300" height="210" style="float: left; margin: 0 15px 15px 0;" />
    <p>As part of our <strong>internship placement programme</strong>, we will find you a suitable worksite in Cape Town based upon your academic goals, skill set, and preferences. Do you love <strong>working with kids</strong>? No problem! Are you a <strong>premedical student</strong> interested in gaining a global perspective on medicine in sub-Saharan Africa? What else would you like to do? We have a place for you! </p>
	<p>While other internship placement organisations in Cape Town charge several thousand dollars, much of which is spent on administrative costs and overhead, ThinkTwice reinvests your low-cost internship placement fee back into work done in South Africa to better the lives of everyone. </p>

    <div class="dash"></div>

    <h1 style="font-family: 'Open Sans', Arial, Helvetica, sans-serif; font-size:22px;"><strong>Cost:</strong> US$750</h1>
	<p><strong>This program fee give you access to ThinkTwice's staff and resources before, during, and after your journey to Cape Town. It includes:</strong>
		<ul>
        	<li>A pre-journey consultation with a ThinkTwice staff member via email or Skype, providing guidance before you even leave your home country. </li>
        	<li>Customized accommodation location service, guaranteeing you will have a comfortable and affordable place to call home throughout your stay in Cape Town. </li>
        	<li>Personalized meet and greet at the airport and transportation to the city upon your arrival in the super awesome ThinkTwice Mobile.</li>
        	<li>Project placement based on your academic needs, preferences, and future goals. </li>
        	<li>Friendly and Professional ThinkTwice assistance during your time of stay. </li>
        	<li>Support of ThinkTwice's work bettering the lives of communities and children. </li>
        	<li>An investment in the lives of South African's children.</li>
        	<li>Financial and structural support for your internship placement site. </li>
        	<li>Transport to the airport when you leave the country.</li>
         </ul>
     </p>
	<p><strong>What you will have to pay for during your stay in Cape Town:</strong>
    	<ul>
        	<li>Rent, usually costing approximately $300 to $400 USD per month. </li>
        	<li>Food... and there are lots of delicious restaurants in Cape Town to try! </li>
        	<li>Personal transport on a day-to-day basis. </li>
        	<li>Leisure activities. </li>
        	<li>Insurance</li>
        	<li>Communication</li>
        	<li>Whatever else you want</li>
       </ul>
     </p>

		<!-- <div class="needsContact">
    	<strong> Why not think about doing your internship with ThinkTwice?!</strong><br />
    	For further details and to apply to our personalized, low-cost internship placement program, contact: <a href="mailto:info@thinktwice.org.za">info@thinktwice.org.za</a>
    </div> -->
		<?php include('../partials/cta_contact.php'); ?>
	<p>We are looking forward to offering you one of the best experiences of your lifetime in one of the most beautiful cities in the world.</p>

</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
