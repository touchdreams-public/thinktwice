<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Volutneer Programme</h1>

    <p>Volunteering is a great reminder of our freedom of choice and expression.

    <p>ThinkTwice is forever grateful to the many volunteers who have sown massively into our projects from inception. These volunteers give freely of their time and skills to ThinkTwice in a number of ways.</p>
    <img src="../images/volunteers01.png" width="300" height="210" style="float: right; margin: 0 0 15px 0;" />
    <p>Many overseas students come to us to complete an internship through experiential learning. Many students (from local communities as well as from overseas) take a "gap year" to "give back to society" by coming to work at ThinkTwice to serve children in various under-resourced communities in South Africa.</p>
    <p>Volunteers are trained to facilitate the ThinkTwice programmes in cr&egrave;ches/schools as well as get opportunities to serve on a ThinkTwice team to run training workshops, where they get to sharpen their presentation and group facilitation skills. In addition, volunteers are able to get involved in various essential administrative tasks such as marketing, research, fundraising, event planning and workshop logistics i.e. recruitment of trainees and preparation for workshops.</p>
    <p>In line with our future plan for expansion and sustainability, ThinkTwice is acting as its own volunteer recruitment and placement programme agency. Your direct support will ensure that ThinkTwice continues to contribute to the empowerment of communities and the safety of children.</p>

		<!-- <p>For participation in our placement programme, please contact Moussa Mulamba directly at <a href="mailto:moussa@thinktwice.org.za ">moussa@thinktwice.org.za</a></p> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
