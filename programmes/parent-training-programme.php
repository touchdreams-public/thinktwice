<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Parent Training Programme</h1>
	 <img src="../images/parents01.png" width="300" height="210" style="float: right; margin: 0 0 15px 0;" />
     <img src="../images/parents02.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px; clear:right;" />
    <p>ThinkTwice endorses the fact that parents are their children's primary caregivers. As such, ThinkTwice believes that parents should be supported to care for their children's emotional, social, intellectual, physical and spiritual needs in this challenging and evolving world.</p>
    <p>ThinkTwice is in the process of developing a complementary Parent Training programme that will ensure that parents of children who are exposed to the Jerry Giraffe programme are reached with the same message. Parents are invaluable to the wellbeing of their children!</p>
    <p><strong>ThinkTwice is available to give talks to parents on the following topics:</strong>
	<ul>
    	<li>Discipline</li>
    	<li>Child participation</li>
    	<li>Self esteem</li>
    	<li>Psycho-social support</li>
    	<li>Child sexual abuse</li>
   	</ul></p>
    <!-- <div class="needsContact" style="clear:both;"><strong>If you are interested in booking a talk with ThinkTwice please contact:</strong><br /> Moussa K. Mulamba at <a href="mailto:moussa@thinktwice.org.za">moussa@thinktwice.org.za</a> or 021 761 3338.</div> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
