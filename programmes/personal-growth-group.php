<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Personal Growth Group</h1>
	<img src="/images/personal01.png" width="300" height="210" style="float: right; margin: 0 0 15px 0;" />
     <img src="/images/personal02.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px; clear:right;" />
    <p>Personal Growth Group (PGG) is a small group experience offered to grade 10-11 learners. The group meets after school hours. The programme offers 11 sessions of 11/2 hours each twice a week. A group of 8-10 learners gather under the guidance of 2 facilitators to discuss issues that affect the learners.</p>
    <p>We want to see the lives of young people in the group being impacted positively from the group experience. We hope that there will be a passing on of skills as members continue to relate with their family, friends and communities.</p>
    <p><strong>PGG aims:</strong>
		<ul>
        	<li>To provide a safe context for teenagers to talk about the issues in their lives</li>
        	<li>To be a positive role model by:
				<ul>
                    <li>Providing correct information</li>
                    <li>Sharing from own lives when appropriate</li>
                    <li>Modelling positive, non-judgmental listening and feedback</li>
                </ul>
            </li>
        	<li>To challenge members' thinking, attitudes and behaviour on arising issues</li>
        	<li>To be a referral base</li>
         </ul>
	</p>
    <!-- <div class="needsContact" style="clear:both;"><strong>If you are interested in having this programme<br />at your school or organisation please contact:</strong><br /> Miemie Saffi at <a href="mailto:miemie@thinktwice.org.za">miemie@thinktwice.org.za</a> or 021 761 3338.</div> -->
		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
