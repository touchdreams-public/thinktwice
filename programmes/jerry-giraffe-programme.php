<!DOCTYPE html>
<html>
<?php include_once('../partials/head.php'); ?>

<body>
	<?php include_once('../partials/header.php'); ?>

		<?php include_once('../partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Jerry Giraffe Programme</h1>
	<img src="../images/jerry01.png" width="300" height="210" style="float: right; margin: 0 0 10px 15px;" />
    <img src="../images/jerry02.png" width="300" height="426" style="float: right; margin: 0 0 10px 15px;  clear: right;" />
	<p>The Jerry Giraffe programme is made up of 15 lessons which teach young children (4 - 6 year old) the following important concepts:
  <ul>
    	<li>family and relationships</li>
        <li>making positive choices</li>
        <li>encouraging good self esteem</li>
        <li>expressing feelings</li>
        <li>HIV/AIDS</li>
        <li>body pride and ownership</li>
        <li>child abuse awareness and safety skills</li>
        <li>modelling positive endings and saying goodbye</li>
    </ul>
    </p>
    <p>The lessons are all interactive and children learn through various methods such as participation, questions and answers, songs, games, chants, icebreakers, reading stories and role-play.</p>
    <p>The Jerry Giraffe programme is designed for use with children in a classroom context. However, its message can be shared with children wherever appropriate. Overall the programme offers educators child-friendly tools and techniques to help maintain discipline and to boost a healthy understanding of learning through structured means while having loads of fun. The Jerry Giraffe programme draws massive attention to giving psycho-social support to children.</p>
    <p>We invite you to place your hand on this invaluable resource by attending the training workshops we offer. </p>

		<?php include('../partials/cta_contact.php'); ?>
</div>
<?php include_once('../partials/footer.php'); ?>
</body>
</html>
