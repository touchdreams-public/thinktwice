<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>The Need</h1>
    <p class="quote">In days gone by, and possibly even today in many instances, the view has prevailed that children should 'be seen and not heard'. The time has come for our children to be seen and to be clearly heard. The cries of our abused and exploited children must no longer fall on deaf ears or on deaf minds.  <em><strong>- Nelson Mandela 1996</strong></em></p>


    <h1>Reality on the ground<br /></h1>
    <h2>Current state, What we are facing</h2>
	<ul>
    	<li>South Africa is known as the rape capital of the world.</li>
    	<li>44% of all sexual offences reported are against children.</li>
    	<li>Sexual abuse cases are reported on almost a daily basis in the media: these are only the tip of the iceberg as most cases go unreported.</li>
    	<li>South Africa's children are faced with challenges daily: poverty; eroded family structures and values; sexist, patriarchal and violent culture.</li>
    	<li>Child sexual abuse is likely to be perpetrated by family members or those known to the child.</li>
    	<li>Child sexual abuse occurs across all demographic, ethnic and family groups and in both girls and boys.</li>
    	<li>Child sexual abuse has long-term consequences for the child: guilt and shame; academic problems; physical problems; emotional disorders; interpersonal difficulties.</li>
    	<li>Prevention must occur at three levels: primary, secondary and tertiary. Primary prevention is the most cost effective and is directed at all children before abuse occurs. </li>
    </ul>

    <div class="dash"></div>

	<h1>We have common responsibilities</h1>
 	<h2>The Children's Act 38 of 2005 amended March 2010 section 110 (1) provides guidelines for reporting abuse:</h2>
	<p><img src="images/reality.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px;" />"Any correctional official, dentist, homeopath, immigration official, labour inspector, legal practitioner, medical practitioner, midwife, minister of religion, nurse, occupational therapist, physiotherapist, psychologist, religious leader, social service professional, social worker, speech therapist, teacher, traditional health practitioner, traditional leader or member of staff or volunteer worker at a partial care facility, drop-in centre or child and youth care centre who on reasonable grounds concludes that a child has been abused in a manner causing physical injury, sexual abuse or deliberately neglected, must report that conclusion in the prescribed from to a designated child protection organisation, the provincial department of social development or a police official."</p>
 	<p>Therefore it would be a criminal offence not to assist our children by reporting child abuse; in this case, child sexual abuse. ThinkTwice urges you to stand and raise our collective voice through child safety programmes, training-workshops, campaigns, advocacy and relevant joint efforts to protect our children.</p>

</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
