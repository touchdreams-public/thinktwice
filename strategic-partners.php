<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Strategic Partners</h1>

    <p>ThinkTwice's stakeholders form an essential role in the functioning of the organisation and its various programmes. </p>
    <p>The organisation is funded through local and overseas donors, corporate sponsors and government departments. These sponsors provide monetary funding to pay staff salaries and office overhead expenses as well as direct programme implementation costs such as programme resources and materials, transport to delivery sites, programme management, facilitation, and monitoring. Some sponsors provide support in the form of donations-in-kind such as office furniture and office refreshments as well as website and email hosting. ThinkTwice is fully accountable to various stakeholders and provides reports and audited financial statements when requested.</p>
    <p>ThinkTwice has collaborative partnerships with some organisations, many of which are also funders. These partnerships have been directly involved in the development of programmes and the implementation processes followed by programme staff. The table below outlines the sponsoring organisations and collaborative partnerships and the contributions they are making to TT.</p>

		<p class="logos" style="text-align:center;">
			<img src="/images/logos/etdp-seta-logo.jpg" style="width: 165px;"/>
			<img src="/images/logos/Noupoort_Wind_Farm_logo.png" style="width: 165px;" />
			<img src="/images/logos/SA_lotto_logo.png" style="width: 135px;" />

			<!-- <img src="/images/logos/logo-Community-Chest.gif" style="width: 165px;" /> -->
			<!-- <img src="/images/logos/Logo-Cry.gif" style="width: 165px;" /> -->
			<img src="/images/logos/H2O-logo.jpg" style="width: 140px;" />
			<img src="/images/logos/logo-Social-department.gif" style="width: 165px;" />
			<img src="/images/logos/logo-Comtel.png" style="width: 165px;" />
			<img src="/images/logos/logo-Fikelela-AIDS.gif" style="width: 60px;" />
			<!-- <img src="/images/logos/logo-Jubilee.gif" style="width: 165px;" /> -->
			<img src="/images/logos/logo-Willowton.gif" style="width: 165px;" />
			<!-- <img src="/images/logos/Logo-Austrailian-AIDS.gif" style="width: 110px;" /> -->
			<img src="/images/logos/logo-Connect.gif" style="width: 165px;" />
			<!-- <img src="/images/logos/logo-Fikelele.png" style="width: 165px;" /> -->
			<img src="/images/logos/logo-City-of-CT.gif" style="width: 165px;" />
			<!-- <img src="/images/logos/logo-Connect.png" style="width: 165px;" /> -->
			<img src="/images/logos/logo-Grand-Slots.gif" style="width: 165px;" />
			<img src="/images/logos/logo-NPA.gif" style="width: 140px;" />
			<img src="/images/logos/Logo-Unitrans.jpg" style="width: 160px;" />
			<img src="/images/logos/logo_h3o.jpg" style="width: 120px;" />
			<!-- <hr />
			<img src="/images/logos-partners.gif" /> -->
		</p>

		<p>
    	<table cellpadding="5" cellspacing="0" border="0">

      		<tr>
	        	<td width="40%" style="border-bottom: 1px solid #000000;"><h2>STAKEHOLDER</h2></td>
	          <td width="60%" style="border-bottom: 1px solid #000000;"><h2>CONTRIBUTION</h2></td>
          </tr>
          <tr>
          	<td><strong>Hannah Schaefer</strong></td>
              <td>Local young lady providing financial support, various fundraising and other services.</td>
          </tr>
          <tr>
          	<td><strong>Alexander and Kate Bates</strong></td>
              <td>Overseas friends providing financial support for operational expenses.</td>
          </tr>
          <tr>
          	<td><strong>Bowley Charitable Trust</strong></td>
            <td>Provides funding.</td>
          </tr>
          <tr>
          	<td>
	          	<strong>City of Cape Town (Health Directorate)</strong><br /></td>
              <td>Provides networking opportunities for strategic partnerships through its Multi-Sectoral Action Teams (MSATs).</td>
          </tr>
          <tr>
          	<td>
	          	<strong>Comtel</strong><br /></td>
              <td>Corporate donor who provides web and email hosting to the organisation.</td>
          </tr>
          <tr>
          	<td>
         	  	<strong>Connect Network</strong><br /></td>
              <td>Supports and facilitates partnering with other organisations on joint projects; provides quality improvement and accreditation through Quality Improvement System, an international standard for non-profit organisations. Provides occasional financial support.</td>
          </tr>
          <tr>
          	<td>
	          	<strong>Department of Social Development</strong><br /></td>
              <td>Provides funding and supports implementation of programme and training, mainly through its databases of registered early childhood development sites.</td>
          </tr>
          <tr>
          	<td>
            	<strong>Encounter Church</strong><br /></td>
              <td>Local church providing mentorship and spiritual support for staff. Provides financial support.</td>
          </tr>
          <tr>
          	<td>
							<strong>Evangelisch-reformierte Kirche des Kantons St.Gallen and Rappeswil-Jona</strong></td>
              <td>Provides funding for operational expenses (Swiss donors).</td>
          </tr>
          <tr>
          	<td>
         	  	<strong>Fikelela AIDS Project</strong><br /></td>
              <td>Local NPO that has partnered with ThinkTwice to develop and implement the Jerry Giraffe Sunday School programme.</td>
          </tr>
          <tr>
          	<td>
         	    <strong>Grand Slots</strong><br /></td>
              <td>Corporate donor who provides project-specific funding for delivery.</td>
          </tr>
          <tr>
          	<td>
            	<strong>H2O International</strong><br /></td>
              <td>Corporate donor who provides purified water to ThinkTwice offices.</td>
          </tr>
          <tr>
          	<td>
            	<strong>L D Gray</strong><br /></td>
              <td>Individual who provided funds</td>
          </tr>
          <tr>
          	<td>
         	  	<strong>National Lottery Distribution Trust Fund</strong><br /></td>
              <td>Provides funding.</td>
          </tr>
          <tr>
          	<td>
              	<strong>National Prosecuting Authority - Sexual offences</strong><br /></td>
              <td>Provides information relating to legal issues and protocol regarding child sexual abuse.</td>
          </tr>
          <tr>
          	<td>
            	<strong>Noupoort Wind Farm</strong><br /></td>
              <td>Provides funds for community development projects in Noupoort / Northern Cape</td>
          </tr>
          <tr>
          	<td>
              	<strong>Unitrans</strong><br /></td>
              <td>Corporate donor who provides funding for operational expenses.</td>
          </tr>
          <tr>
          	<td style="border-bottom: 1px solid #000000;">
              	<strong>Willowton Oil Foundation</strong><br /></td>
              <td style="border-bottom: 1px solid #000000;">Local CSI corporate providing financial support for projects.</td>
          </tr>
        </table>
    </p>

    <div>

    </div>
    <a name="donate"></a>
    <p>All donations to us are fully tax deductible and no Donations Tax is payable as we are a registered PBO with SARS in terms of Section 18(A) of the Income Tax Act.</p>

    <div class="ngo">
        <strong style="color:#ed6d2c;">Our registration details are as follows: </strong>
        <br /><br />
        <strong style="color:#0e76bc;">Non-Profit Organisation Registration number:</strong> 031-678 NPO
        <br />
        <strong style="color:#0e76bc;">Public Benefit Organisation (PBO) Reference number:</strong> 930011769
    </div>
    <p><strong style="color:#ed6d2c;">Our banking details are as follows: </strong></p>
    <p><strong>Account Name: </strong>ThinkTwice<br />
        <strong>Account Number:</strong> 6238 0088 213<br />
        <strong>Account Type:</strong> Business Cheque Account<br />
        <strong>Bank:</strong> FNB (First National Bank)<br />
        <strong>Branch:</strong> Claremont<br />
        <strong>Branch Code:</strong> 200 109<br />
        <strong>IFT Swiftbic code:</strong> FIRNZAJJXXX
  </p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
