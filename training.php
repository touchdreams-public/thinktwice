<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>

  	<div class="contentContainer">

    	<h1>Training</h1>

      <ul>
        <li><a href="downloads/programmes/Facilitator_Training_NQF5.pdf" target="_blank">
					Facilitator Training NQF5</a></li>
        <li><a href="downloads/programmes/Assessor_Training_NQF5.pdf" target="_blank">
					Assessor Training NQF5</a></li>
        <li><a href="downloads/programmes/Moderator_Training_NQF6.pdf" target="_blank">
					Moderator Training NQF6</a></li>
        <li><a href="downloads/programmes/Early_Childhood_Development_NQF4.pdf" target="_blank">
					Early Childhood Development NQF4</a></li>
        <li><a href="downloads/programmes/Development_Practice_NQF5.pdf" target="_blank">
					Development Practice NQF5</a></li>
      </ul>

    </div>
    <?php include_once('partials/footer.php'); ?>
  </div>
</body>
</html>
