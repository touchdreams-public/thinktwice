<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

	<?php include_once('partials/nav.php'); ?>

	<div class="contentContainer">
		<h1>Monitoring &amp; Evalutaion</h1>

    <h2>Monitoring</h2>
  	<p>ThinkTwice conducts regular monitoring of all programme activities. All information and feedback pertaining to programme delivery is captured and collated for reporting to the management, board and relevant funders and other stakeholders.</p>

		<div class="dash"></div>

	  <h2>Theory &amp; Outcome Evaluation: 2013</h2>
		<p>A formative evaluation was undertaken of the Jerry Giraffe programme during 2013 as part of a Masters degree in Programme Evaluation at UCT. It was conducted by Deirdre Solomon.</p>

	  <p>
			<a href="downloads/Summary-2013-Evaluation-Report.pdf" target="_blank">Summary of these findings (pdf)</a><br />
			<a href="downloads/Detailed-2013-Evaluation-Report.pdf" target="_blank">More detailed findings document (pdf)</a>
		</p>

		<!-- <div class="dash"></div> -->

		<!-- <h2>Impact Evaluation: 2014/2015</h2>
		<p>ThinkTwice is currently sending out proposals so that the findings of the evaluation conducted in 2013 can be expanded upon through randomised controlled trials that include control or comparison groups and pre- and post-tests to establish programme impact on both the trained teachers and children. This planned evaluation will involve experimental research design.</p>
		<p><a href="downloads/Planned-2014_2015-Impact-Evaluation.pdf" target="_blank">Further details of this planned evaluation (pdf)</a></p> -->

	</div>
	<?php include_once('partials/footer.php'); ?>
</body>
</html>
