<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Testimonials: Educators / Principals</h1>
    <img src="images/testimonials-educators01.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px;" />
	<p>The children were 100% immersed in the presentations and most of them interacted fully. Best of all was watching the shy one's interacting. The facilitators understood the children and they always treated them with respect and understanding.<br /><strong>- The Children's Place educare</strong></p>
    <p>We principal, staff and learners at Madrassa Tal balaagh's wish to thank ThinkTwice for the most interesting program rendered at our Educare. Due to our experience with this program, would we suggest that every educare gets trained in it. <br /><strong>- Madrassa Tal balaagh's</strong></p>
    <p>Everything was excellent, the highlight being HIV and Aids lesson that has the story about the story about Aunty Precious in Hospital. The children grew fond and attached to the Jerry Giraffe character. <br /><strong>- Olga's Educare</strong></p>
    <p>When it comes to handling a disclosure, I will not ask the child many questions or cross examine the Child's statement. I will listen attentively to the child and there after follow the school's disclosure policy. <br /><strong>- Beaconvale Community care centre</strong></p>
    <img src="images/testimonials-educators02.png" width="300" height="210" style="float: left; margin: 0 15px 5px 0;" />
    <p>"Jerry and the talking flower" created a platform for discipline which I found as principal of the cr&egrave;che amazing especially with their behavior and attitude among their peers. We are faced with parents who are secretive with issues of abuse! The implementation of the programme is much needed as we are faced with different forms of abuse in the community. <br /><strong>- Mizpah Educare</strong> </p>
    <p>The manner in which the think twice team presented the lesson was informal and enjoyable to all the children who participate. The participation and interaction were encouraged and the children were extremely happy to oblige. The program taught the children various values and take pride in their bodies and that their bodies are sacred and not to be abused by anyone. <br /><strong>- Fifth Avenue Day Care Centre</strong></p>

</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
