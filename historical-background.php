<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Historical Background</h1>
	<p>ThinkTwice, previously known as youth with a vision (1996 – 2003). began as the prevention programme of the Pregnancy Help Centre (PHC) in Cape Town in 1996 to meet the growing demand for women and teenagers experiencing the crisis of an unplanned pregnancy.
	<p>Women received counselling and assistance with their decisions through the Crisis Counselling intervention programme. The youth with a vision team visited high schools and implemented the Undiluted and personal growth groups prevention programmes. On the other hand, the prevention programme called Youth with a Vision, visited high schools and implemented Undiluted and Personal Growth Groups (PGGs). The aim of these lifestyle education programmes was to prevent the spread of HIV, STIs and unplanned teenage pregnancies.</p>
	<!-- <p style="text-align:center;"><img src="images/History.gif" width="550" height="330" /></p> -->
	<p style="text-align:center;"><img src="images/ThinkTwice_Pregnancy_Help_Centre_PHC_Diagram.png" /></p>
  <p>In 2002, through its interactions with high school learners, the prevention team realised that lifestyle changes needed to start much earlier in children's lives. Teenagers already had set beliefs about themselves which were hard to change. In addition to this, we received requests from primary schools to run Undiluted and PGGs amongst their learners. This highlighted the need for the prevention programme to be targeted at a much younger age and lead to the development of life skills programmes for primary school children.</p>
  <p>With its broadened focus of attention to include primary school children as well as having evolved away from just the prevention of unplanned pregnancy amongst teenagers, the PHC felt that both programmes would benefit if we operated separately from each other. Therefore, in 2004, the prevention programme was registered as a separate non-profit organisation and ThinkTwice was created. </p>
  <p>At the end of 2009, as a result of a strategic meeting, ThinkTwice decided to focus on the pre-school sector of our education. However, high school as well as parents programmes are still being delivered on demand and/or where there are opportunities.</p>
	<p>In 2015 we were accredited with the ETDP SETA (ETDP10873)</p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
