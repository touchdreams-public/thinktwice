<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>

  	<div class="contentContainer">

    	<h1 style="font-size:60px; text-align:center;">Strategy</h1>

      <h2>Develop</h2>
      <p>“We develop appropriate skills training programs, resources and learning material to meet the needs of children, educators, care givers and community workers at large.”</p>
      <h2>Train & Implement</h2>
      <p>“We equip, empower and Mentor educators, caregivers and Community workers at large for personal development, active citizenship for the betterment of their workplace and community.”</p>
      <h2>Collaborate</h2>
      <p>“We collaborate with other organisations, Institutions, funders, Governmental departments, corporate and interested parties who also have a vision to see children living and learning in nurturing and safe environments”</p>

    </div>
    <?php include_once('partials/footer.php'); ?>
  </div>
</body>
</html>
