<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>

  	<div class="contentContainer">

    	<h1>Community Development (Noupoort)</h1>

			<p data-align="center">
				<img src="images/projects/Noupoort2.jpg" />
			</p>

			<h2>Background</h2>
			<p>Using a scientifically proven model of change called Inclusive Neighborhood Spaces (INS), young people identify challenges that children and young people face and initiate community based projects to mitigate those challenges. They organized themselves and registered a Non - Profit Company called Inclusive Neigbourhood Spaces Noupoort 129-639-NPO with the department of social development. All their activities are conducted under the hashtag #BringNoupoortBack.</p>

			<p data-align="center">
				<img src="images/projects/Noupoort1.jpg" width="600" />
			</p>

			<h2>Projects for 2018</h2>

			<h3>1. Aftercare and Child Protection Centre</h3>
			<p>The INS team works with children that have been most affected by poverty and dealing with that trauma daily. They have identified and selected children from ECD centres and primary schools in Noupoort.</p>
			<p>They do the following activities with the children: Play games (puzzles, lego, etc), literacy programme and numeracy programme, homework assistant, feeding and bathing them daily from Monday to Friday.</p>
			<p>Furthermore, children are provided with therapeutic intervention and counselling in order to begin the process of healing and restoration from their difficult and traumatic home circumstances.</p>
			<p>Play therapy is used  to communicate emotions, feelings, experiences and behaviour. The counsellor uses the responses in play to intervene and to heal.</p>

			<h3>2. Back to school project</h3>
			<p>There is a significant number of Primary School going children that are not in schools for known as well as unknown reasons in Noupoort. These children loiter around town begging for money from people that have come to shop. These children are growing up without any education and will become a big  problem to the community of Noupoort if they have not started already. Moreover there are some children that have dropped out of high school.</p>
			<p>The aim with this programme is to begin getting these children off the streets and educating them so that they can catch up with their peers in schools. The main aim is to get them back into the school system and to have no child out of school by 2020.</p>

			<h3>3. Driver Education Programme</h3>
			<p>This programme aims to upskill out of school, unemployed youth with driving skills.</p>
			<p>This then makes the young people who have not had an opportunity for tertiary education to become employable.</p>

			<h3>4. School cleanup and vegetable garden maintenance</h3>
			<p>The toilets at the primary schools are filthy and completely unhygienic for learners to use. Most are not in working condition and the smell is unbearable. This project aims to conduct a monthly cleanup of the school toilets and classrooms. Unblock the ones that are blocked. Furthermore, due to water restrictions educate the learners how to use the toilet with limited water.</p>

			<h4>Vegetable garden:</h4>
			<p>Open spaces at Schools and ECD centers were identified and used to plant vegetables to supply to the ECD centres as well as community members. This is a minor sollution to alleviate poverty in the community.</p>

			<h3>5. Education Quality Control Forum</h3>
			<p>The Education Quality Control forum is a body that holds the ECD centres accountable to the quality standards agreed upon by all members of the forum and advocate for children’s issues in their Centres.</p>
			<p>The forum hosts awards annually to elevate the standard of education in ECD centres to an admirable level.</p>
			<p>The forum nominates annual winners for ECD best practice in the following categories:</p>

			<ul>
					<li>Best outcome-based educational programme</li>
					<li>Best nutritional programme</li>
					<li>Best adherence to the Child care Act and child protection policies</li>
					<li>Best Ecd center teacher of the year</li>
					<li>Best Ecd Principal of the year</li>
					<li>Most Active and supportive Ecd school governing body</li>
			</ul>

			<p>The forum also hosts an annual march for child protection in the child protection month of May.</p>

			<h3>6. Annual Children's Olympics</h3>
			<p>INS hosts annual children’s Olympics.</p>
			<p>The children’s Olympics is fun games and races for children aged 3-5 for the ECD centres as well as grade R to Grade 3 from the 3 primary schools. This totals to a little over 1000 children having fun in the sun.</p>
			<p>Their games include hola hoop contest, tyre race, water balloon race, egg on a spoon race, 3 legged race and hop-in-a-sack race.</p>
			<p>All the children receive medals for their participation whether in the games or in the cheering and singing.</p>

    </div>
    <?php include_once('partials/footer.php'); ?>
  </div>
</body>
</html>
