$(document).ready(function(){
  // $('#slider').nivoSlider();

  $('.carousel').slick({
    autoplay: true,
    dots: true,
    arrows: false,
    prevArrow: '<button type="button" class="slick-prev">&lang;</button>',
    nextArrow: '<button type="button" class="slick-next">&rang;</button>'
  });
});
