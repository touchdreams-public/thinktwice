<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Look At The Literature</h1>
    <p>South Africa is reported to be the rape capital of the world (Mapenzauswa, 2013) where, between May 2011 and April 2012, more than 64000 sexual offences were reported; of these more than 25000 were crimes against children. According to Mapenzauswa, (2013), only a fraction of sexual offences are reported; the true statistics are suggested to be higher. As reported by Dawes and Mushwana (2007), it is difficult to obtain accurate data on CSA as it is "an illicit and secret activity" and incidence studies and registers of child sexual abuse offences only measure the "very tip of the iceberg" (p. 281). There is evidence that many cases go unreported and undetected and that most CSA victims fail to disclose the abuse (Dawes &amp;amp; Mushwana, 2007; Finkelhor, 1990; Jewkes &amp; Abrahams, 2002).  Table 1 documents violence against children in South Africa for the period 2004 to 2005 (Richter &amp;amp; Dawes, 2008). Children are victims in almost 50% of all reported cases of indecent assault and rape.    <p>
      <img src="images/literature.png" width="300" height="210" style="float: right; margin: 0 0 15px 15px;" />
    <p>Child sexual abuse has been reported on a daily basis in popular media in South Africa (<a href="http://www.iol.co.za">www.iol.co.za</a>; <a href="http://www.news24.co.za">www.news24.co.za</a>). The following three alleged cases of sexual offences which were perpetrated against children during the month of March 2013 illustrate the gravity of the problem of  CSA: a four-month old baby, raped by a 54-year-old man in Cape Town (News24, 2013, March 20), a seven-year old boy, sexually molested by his 32-year-old step-father in Pietermaritzburg (Oellermann, 2013, March 28), and a six-year old girl,  raped by her father in Pietermaritzburg (The Witness, 2013, March 20). </p>
    <p>South Africa's rape, sexual assault and CSA are major societal problems which have resulted from factors considered specific to this country (Richter &amp; Dawes, 2008). Factors reported to contribute to the high incidence of sexual offences include widespread poverty and the resulting pressures and deprivation this brings to poor people.  Apartheid eroded family structures and resulted in deep-rooted inequalities across all spheres of life (Loffell, 2004). Challenges of South Africa's sexist, patriarchal and violent culture, where men view themselves as superior to women and children, have been reported as a major contributory factor (Jewkes &amp;amp; Abrahams, 2002; Loffell, 2004; Richter &amp;amp; Dawes, 2008). </p>
    <p>Zwi, Woolfenden, Wheeler, O'Brien, Tait and Williams (2007) reported that risk factors that contribute to the incidence of CSA include domestic violence, poor parental attachment, parental alcoholism, and social isolation of girls; these factors are reported to be rife in South Africa (Jewkes &amp; Abrahams, 2002; Loffell, 2004). The "absence of protective adults at home, unemployment, and a history of drug abuse among the perpetrators" were found by Weatherley, Siti Hajar, Noralina, John, Preusser and Yong (2011, p. 119) to be the leading factors associated with CSA in Malaysia. These are additional risk factors to be taken into account in the South African context (Jewkes &amp;amp; Abrahams, 2002).</p>
    <img src="images/photo02.png" width="300" height="210" style="float: left; margin: 0 15px 15px 0;" />
    <p>Perpetrators of CSA are most likely to be family members or family acquaintances known to the child (Elliott, Browne, &amp; Kilcoyne, 1995; Wurtele, Saslawsky, Miller, Marrs &amp; Britcher, 1986; Zwi et al., 2007). There have been reports of CSA across all demographic, ethnic and family groups, in both girls and boys, and perpetrators include those outside the family as well as within it (Zwi et al., 2007). Child sexual abuse has serious and long-term consequences for children's development and many victims carry feelings of guilt, shame and responsibility for their victimisation throughout their childhood and adult lives (Finkelhor, 1990; Kenny, 2010; Topping &amp;amp; Barron, 2009). This feeling of shame is reported to "often result in an array of difficulties, including emotional disorders (e.g. depression, anxiety), cognitive disturbances (e.g., poor concentration, dissociation), academic problems, physical problems (e.g., sexually transmitted diseases, teenage pregnancy), acting-out behaviours (e.g., prostitution, running away from home), and interpersonal difficulties" (Kenny, 2010, p. 981).</p>
    <p>According to Topping and Barron (2009), CSA prevention strategies require three specific kinds of interventions, namely primary, secondary and tertiary. Primary intervention is directed at all children before abuse occurs and is considered the most cost effective. Secondary prevention is directed at those who are at greater than average risk for abuse and tertiary prevention focuses on those children who have already been abused, as well as those who have perpetrated abuse (Topping &amp;amp; Barron, 2009). </p>

	<div class="dash"></div>

    <h2>References</h2>
    <div style="font-size:11px; line-height:normal;">
    <p>Dawes, A. &amp; Mushwana, M. (2007). Monitoring child abuse and neglect. In A. Dawes, R. Bray &amp; A. van der Merwe (Eds.), Monitoring child well-being: A South African rights-based approach (pp. 269-292). Cape Town: HSRC Press.</p>
    <p>Elliott, M., Browne, K., &amp; Kilcoyne, J. (1995). Child sexual abuse prevention: What offenders tell us. Child Abuse &amp; Neglect, 19(5), 579-594. doi:10.1016/0145-2134(95)00017-3 </p>
    <p>Finkelhor, D. (1990). Early and long-term effects of child sexual abuse: An update. Professional Psychology: Research and Practice, 21(5), 325-330. doi:10.1037/0735-7028.21.5.325 </p>
    <p>Jewkes, R., &amp; Abrahams, N. (2002). The epidemiology of rape and sexual coercion in south africa: An overview. Social Science &amp; Medicine, 55(7), 1231-1244. doi:10.1016/S0277-9536(01)00242-8</p>
    <p>Kenny, M. C. (2010). Child sexual abuse education with ethnically diverse families: A preliminary analysis. Children and Youth Services Review, 32(7), 981-989. doi: 10.1016/j.childyouth.2010.03.025 </p>
    <p>Loffell, J. (2004). Policy responses to child sexual abuse in South Africa. In L. Richter, A. Dawes, &amp; C. Higson-Smith (Eds.), Sexual abuse of young children in southern Africa (pp. 250-262). Cape Town: HSRC Press.</p>
    <p>Mapenzauswa, S. (2013, February 6). Outcry over India gang rape shames some in South Africa. Reuters. Retrieved from <a href="http://www.reuters.com">www.reuters.com</a></p>
    <p>News24. (2013, March 20). Cape Town baby rape case transferred. News24. Retrieved from <a href="http://www.news24.com">www.news24.com</a></p>
    <p>Oellermann, I. (2013, March 28). 'Molester' off to in-laws. The Witness. Retrieved from <a href="http://www.witness.co.za">www.witness.co.za</a></p>
    <p>Richter, L. M., &amp; Dawes, A. R. L. (2008). Child Abuse in South Africa: Rights and Wrongs. Child Abuse Review, 17(2), 79-93. doi: 10.1002/car.1004 </p>
    <p>The Witness. (2013, March 20). In court: Father accused of raping girl (6). The Witness. Retrieved from <a href="http://www.witness.co.za">www.witness.co.z</a></p>
    <p>Topping, K. J., &amp; Barron, I. G. (2009). School-based child sexual abuse prevention programs: A review of effectiveness. Review of Educational Research, 79(1), 431-463. </p>
    <p>Wurtele, S.K., Saslawsky, D.A., Miller, S.R., Marrs, S.R., &amp; Britcher, J.C. (1986). Training personal safety skills for potential prevention of sexual abuse: A comparison of treatments. Journal of Consulting and Clinical Psychology, 54 (5), 688-692.</p>
    <p>Zwi, K.J., Woolfenden, S.R., Wheeler, D.M., O'Brien, T.A., Tait, P., &amp; Williams, K.W. (2007). School-based education programmes for the prevention of child sexual abuse. Cochrane Database of Systematic Reviews, (3) doi: 10.1002/14651858.CD004380.pub2 </p>
	</div>

</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
