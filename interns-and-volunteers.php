<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Testimonials: Interns / Volunteers</h1>
	<img src="images/testimonials-interns01.png" width="300" height="210" style="float: right; margin: 0 0 15px 15px;" />
	<p>"My time here at ThinkTwice has changed my outlook on some things. Teaching young kids worried me in the beginning. There I was with no previous experience teaching kids and I made it! I was given this great opportunity to work with amazing people. Those children and their teachers have so much love to offer and you cannot help but give it back. </p>
    <p>Meeting amazing people, getting to know the children I worked with, exploring one of the most beautiful cities in this world: all of this is just the beginning of what my experience in Cape Town was. What ThinkTwice does is just outstanding. I am proud to be able to say I was part of it." <br /><strong>- Vedrana</strong> </p>
    <div class="dash"></div>
    <p>"Due to other commitments in the beginning it was unclear whether or not I would be able to become part of the ThinkTwice team. God however knew my heart and made it possible for me to be a part of this great opportunity. I have been able to learn and grow in areas that I have needed to. I have felt a part of the team and also believe that I have not only worked alongside many of you but also made friends through my time here. (I know Deirdre will employ me some day) It has been beautiful to be a part of a team that is so passionate about the work that God is doing within you all of you. So once again Thank you for all the blessings you have poured out onto my life. Blessings for the road ahead. <br /><strong>- Emma-Jane Shaw J</strong></p>
    <div class="dash"></div>
    <p>"Interning at Think Twice was a rewarding experience, for both personal and professional reasons. There was easy camaraderie amongst team members and this facilitated an enjoyable working environment. At the same time, constant access to immediate superiors and feedback sessions kept one getting work done and executing tasks as required. Think Twice therefore showcased a unique brand of professionalism, characterised by both pleasant fellowship and professional efficacy." <br /><strong>- Shuvai</strong></p>
	<div class="dash"></div>
    <img src="images/testimonials-interns02.png" width="300" height="210" style="float:left; margin: 0 15px 5px 0;" />
    <p>"The staff at ThinkTwice is really passionate about what they do and through their experiences they all have a sense of what the child living in South Africa today needs and has the right to have. With my psychology and teaching background I was truly impressed with the content and implementation of the workshops and programmes and I feel ThinkTwice is doing a fantastic job for our children. The programmes have a great balance of fun and discipline and I am sure that children enjoy Jerry Giraffe and Mr Wiggly Worm coming into their lives to teach them about how special they are and how to treat others like stars. </p>
    <p>When I went on a workshop with Moussa and MieMie I really saw how necessary ECD and parent workshops are as there are still many people who are unaware of issues surrounding HIV and child abuse as well as how to instill a sense of self-esteem and self-worth in children in order to protect them and lay the foundations for a healthy, empathic generation.</p>
    <p>I came to ThinkTwice wanting to give back, and I really have been given the opportunity to do that, however I have been given something in return. I have made new friends and have been shown love by children and educators that have done the workshop.</p>
    <p>I have enjoyed my time at ThinkTwice very much and will take the experiences I have had into the future with me." <br /> <strong>- Adrienne</strong></p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
