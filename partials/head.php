<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="keywords" content="children, charity, non profit, nonprofit, NGO, HIV, AIDS, education, sexual abuse, abuse, south, africa, south africa, cape, town, cape town">
  <meta name="description" content="ThinkTwice, Respect, Hope, Choices, Self-Worth">


  <title>Think Twice</title>
  <link rel="stylesheet" type="text/css" href="/css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="/css/thinktwice.css" />
  <link rel="stylesheet" type="text/css" href="/css/default.css" />
  <link rel="stylesheet" type="text/css" href="/css/nivo-slider.css" />
  <link rel="stylesheet" type="text/css" href="/libraries/slick-1.8.1/slick/slick.css" />
  <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans'>
  <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Roboto+Condensed'>

  <!-- <script language="javascript" type="text/javascript" src="js/jquery-1.7.1.min.js"></script> -->
  <script language="javascript" type="text/javascript" src="/libraries/jquery/jquery-3.3.1.js"></script>
  <!-- <script language="javascript" type="text/javascript" src="libraries/jquery/jquery-3.3.1.min.js"></script> -->

  <script language="javascript" type="text/javascript" src="/js/thinktwice.js"></script>
  <script language="javascript" type="text/javascript" src="/contactform.js"></script>
  <script language="javascript" type="text/javascript" src="/libraries/slick-1.8.1/slick/slick.js" /></script>
  <!-- <script language="javascript" type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script> -->
</head>
