<nav>
  <ul>
      <li>
          <a href="/">Home</a>
          <ul class="submenu">
            <li><a href="/objectives-and-values.php">Our Objectives &amp; Values</a></li>
            <!-- <li><a href="strategy.php">Strategy</a></li> -->
            <li><a href="/historical-background.php">Historical Background</a></li>
          </ul>
        </li>
        <li>
          <a href="/staff.php">Our Team</a>
          <!-- <span>Our Team</spa>
          <ul class="submenu">
            <li><a href="staff.php">Staff</a></li>
            <li><a href="board-members.php">Board Members</a></li>
          </ul> -->
        </li>
      <li>
        <span>Need Assessment</span>
        <ul class="submenu">
          <li><a href="/reality.php">Reality</a></li>
          <li><a href="/look-at-the-literature.php">Look At The Literature</a></li>
        </ul>
      </li>
      <li>
        <span>What we Do</span>
        <ul>
          <li><a href="/training.php">Accredited Training</a></li>
          <li>
            <span>Child Protection programmes</span>
            <ul class="submenu">
              <li><a href="/programmes/jerry-giraffe-programme.php">Jerry Giraffe Programme</a></li>
              <li><a href="/programmes/jerry-giraffe-sunday-school-programme.php">Jerry Giraffe Sunday School Programme</a></li>
              <li><a href="/programmes/teacher-training-programme.php">Teacher Training Programme</a></li>
              <li><a href="/programmes/parent-training-programme.php">Parent Training Programme</a></li>
              <li><a href="/programmes/undiluted-programme.php">Undiluted Programme</a></li>
              <li><a href="/programmes/personal-growth-group.php">Personal Growth Group</a></li>
            </ul>
          </li>
          <li><a href="/community-development-noupoort.php">Community Development (Noupoort)</a></li>
            <li><a href="/courses.php">Online training courses</a></li>
        </ul>
      </li>
      <li><a href="/monitoring-and-evaluation.php">M&amp;E</a></li>
      <li>
        <span>Impact</span>
        <ul class="submenu">
            <li><a href="/interns-and-volunteers.php">Interns / Volunteers</a></li>
            <li><a href="/trainees.php">Trainees</a></li>
            <li><a href="/educators-and-principals.php">Educators / Principals</a></li>
            <li><a href="/other-partners.php">Other partners</a></li>
            <li><a href="/ncpw.php">National Child Protection Week</a></li>
         </ul>
      </li>
      <li><a href="/gallery.php">Gallery</a></li>
      <li><a href="/programmes/volunteer-programme.php">Volunteer</a></li>
      <li><a href="/strategic-partners.php">Strategic Partners</a>
    </ul>
</nav>
