<div class="pre-footer">
	<span class="registration">ETDP10873</span>
	<img src="/images/logos/etdp-seta-logo-horizontal.jpg" />
</div>
<footer role="contentinfo" class="footer">
	<div class="footerBlock">
		CONTACT: Marcia Sibara
    &nbsp;&nbsp;| &nbsp;&nbsp; Moussa: <a href="tel:+27835837481">083 583 7481</a><!--TEL: 021 689 8331 -->
    &nbsp;&nbsp;| &nbsp;&nbsp; Lana: <a href="tel:+27744196331">074 419 6331</a><!--CELL: 081 387 9895 --> <!--083 583 7481-->
    <!-- &nbsp;&nbsp;| &nbsp;&nbsp; FAX: 086 607 9482 -->
    &nbsp;&nbsp;| &nbsp;&nbsp; EMAIL:  <a href="mailto:info@thinktwice.org.za">INFO@THINKTWICE.ORG.ZA</a><br />
    POSTAL/PHYSICAL ADDRESS: 14 PARK ROAD, RONDEBOSCH 7700<br />
    Website by <a href="http://www.crispdesign.co.za" target="_blank">Crisp Design</a>
    &amp; <a href="https://touchdreams.co.za/" target="_blank">Touchdreams</a>
  </div>
</footer>
<!-- <div class="footer" style="height: 30px;"> -->
