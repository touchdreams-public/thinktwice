<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Strategic Partners</h1>

    <p>ThinkTwice�s stakeholders form an essential role in the functioning of the organisation and its various programmes. </p>
    <p>The organisation is funded through local and overseas donors, corporate sponsors and government departments. These sponsors provide monetary funding to pay staff salaries and office overhead expenses as well as direct programme implementation costs such as programme resources and materials, transport to delivery sites, programme management, facilitation, and monitoring. Some sponsors provide support in the form of donations-in-kind such as office furniture and office refreshments as well as website and email hosting. ThinkTwice is fully accountable to various stakeholders and provides reports and audited financial statements when requested.</p>
    <p>ThinkTwice has collaborative partnerships with some organisations, many of which are also funders. These partnerships have been directly involved in the development of programmes and the implementation processes followed by programme staff. The table below outlines the main sponsoring organisations and collaborative partnerships and the contributions they have made to TT from 2012 to the present moment.</p>

    <p>
    	<table cellpadding="5" cellspacing="0" border="0" width="97%">
        	<tr>
            	<td width="40%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;"><h2>STAKEHOLDER</h2></td>
                <td width="60%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000;"><h2>CONTRIBUTION</h2></td>
            </tr>
            <tr>
            	<td>
                	<strong>Australia AIDS South Africa (AASA)</strong><br />
           	    	<img src="images/Logo-Austrailian-AIDS.gif" width="102" height="105" alt="Austrailian AIDS South Africa logo" />
               </td>
                <td>Provides funding for specific delivery projects (Australian funder).</td>
            </tr>
            <tr>
           	  <td>
                	<strong>Care and Relief for the Young Children�s Charity</strong><br />
                	<img src="images/Logo-Cry.gif" width="150" height="59" alt="CRY logo" />
               </td>
                <td>Provides funding for operational expenses (United Kingdom funder).</td>
            </tr>
            <tr>
            	<td><strong>Charitable Trusts</strong></td>
                <td>Provides funding.</td>
            </tr>
            <tr>
            	<td>
                	<strong>City of Cape Town (Health Directorate)</strong><br />
           	    	<img src="images/logo-City-of-CT.gif" width="150" height="109" alt="City of Cape Town logo" />
                </td>
                <td>Provided funding and networking opportunities for strategic partnerships through its Multi-Sectoral Action Teams (MSATs).</td>
            </tr>
            <tr>
            	<td>
                	<strong>Community Chest</strong><br />
           	    	<img src="images/logo-Community-Chest.gif" width="80" height="92" alt="Community Chest logo" />
                </td>
                <td>Local non-profit organisation that provides funding, training and networking opportunities.</td>
            </tr>
            <tr>
            	<td>
                	<strong>Comtel</strong><br />
           	    	<img src="images/logo-Comtel.png" width="150" height="52" alt="Comtel Communications logo" />
                </td>
                <td>Corporate donor who provides web and email hosting to the organisation.</td>
            </tr>
            <tr>
            	<td>
               	  	<strong>Connect Network</strong><br />
           	    	<img src="images/logo-Connect.gif" width="160" height="50" alt="Connect Network logo" />
                </td>
                <td>Supports and facilitates partnering with other organisations on joint projects; provides quality improvement and accreditation through Quality Improvement System, an international standard for non-profit organisations.</td>
            </tr>
            <tr>
            	<td>
                	<strong>Department of Social Development</strong><br />
           	    	<img src="images/logo-Social-department.gif" width="160" height="55" />
                </td>
                <td>Provides funding and supports implementation of programme and training, mainly through its databases of registered early childhood development sites.</td>
            </tr>
            <tr>
            	<td><strong>Evangelical Reformed Church</strong></td>
                <td>Provides funding for operational expenses (Swiss church).</td>
            </tr>
            <tr>
            	<td>
               	  	<strong>Fikelela AIDS Project</strong><br />
           	    	<img src="images/logo-Fikelela-AIDS.gif" width="70" height="135" alt="Fikelela AIDS Project logo" /></td>
                <td>Local NPO that has partnered with ThinkTwice to develop and implement the Jerry Giraffe Sunday School programme.</td>
            </tr>
            <tr>
            	<td>
                	<strong>H2O International</strong><br />
           	    	<img src="images/logo-H2O.gif" width="120" height="71" alt="H2O International logo" />
                </td>
                <td>Corporate donor who provides purified water to ThinkTwice offices.</td>
            </tr>
            <tr>
            	<td>
               	  	<strong>Jubilee Community Church</strong><br />
                	<img src="images/logo-Jubilee.gif" width="150" height="46" alt="Jubilee Community Church logo" />
                </td>
                <td>Provides funding and mentorship support for staff (local church).</td>
            </tr>
            <tr>
            	<td>
               	  	<strong>National Lottery Distribution Trust Fund</strong><br />
                	<img src="images/logo-Lottery.gif" width="90" height="94" alt="National Lottery logo" />
                </td>
                <td>Provides funding.</td>
            </tr>
            <tr>
            	<td>
                	<strong>National Prosecuting Authority � Sexual offences</strong><br />
           	    	<img src="images/logo-NPA.gif" width="120" height="90" alt="National Prosectuting Authority logo" />
                </td>
                <td>Provides information relating to legal issues and protocol regarding child sexual abuse.</td>
            </tr>
            <tr>
            	<td>
               	  <strong>Grand Slots</strong><br />
           	    	<img src="images/logo-Grand-Slots.gif" width="200" height="61" alt="Grand Slots logo" />
                </td>
                <td>Corporate donor who provides project-specific funding for delivery.</td>
            </tr>
            <tr>
            	<td style="border-bottom: 1px solid #000000;">
                	<strong>Willowton Oil Foundation</strong><br />
           	    	<img src="images/logo-Willowton.gif" width="170" height="46" alt="Willowton Group logo" />
                </td>
                <td style="border-bottom: 1px solid #000000;">Corporate donor who provides funding for operational expenses.</td>
            </tr>
        </table>
    </p>

    <p>All donations to us are fully tax deductible and no Donations Tax is payable as we are a registered PBO with SARS in terms of Section 18(A) of the Income Tax Act.</p>

    <div class="ngo">
        <strong style="color:#ed6d2c;">Our registration details are as follows: </strong>
        <br /><br />
        <strong style="color:#0e76bc;">Non-Profit Organisation Registration number:</strong> 031-678 NPO
        <br />
        <strong style="color:#0e76bc;">Public Benefit Organisation (PBO) Reference number:</strong> 930011769
    </div>
    <p><strong style="color:#ed6d2c;">Our banking details are as follows: </strong></p>
    <p><strong>Account Name: </strong>ThinkTwice<br />
        <strong>Account Number:</strong> 6238 0088 213<br />
        <strong>Account Type:</strong> Business Cheque Account<br />
        <strong>Bank:</strong> FNB (First National Bank)<br />
        <strong>Branch:</strong> Claremont<br />
        <strong>Branch Code:</strong> 200 109
    </p>
</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
