<!DOCTYPE html>
<html>
<?php include_once('partials/head.php'); ?>

<body>
	<?php include_once('partials/header.php'); ?>

		<?php include_once('partials/nav.php'); ?>



<div class="contentContainer">
	<h1>Testimonials: Other Partners</h1>
	<h2>FIKELELA</h2>
    <img src="images/logo-Fikelele.png" width="120" height="180" style="float: right; margin: 0 0 15px 10px;" />
	<p>In 2011 A very important and strategic partnership was formed between Fikelela Aids Project and Think Twice. Together we designed a 10 week course for Sunday Schools on child abuse and HIV prevention.  The programme covers the following sessions: teaching children that they are all special - that they all have feelings- how they should treat each other like stars and not like stones. They have choices, and the consequences of their choices. And then it also teaches them body pride - what is a comfortable touch and what is an uncomfortable touch. The programme is fun, using a soft toy called Jerry the Giraffe; the children are all excited to have a visit from Jerry every week.  We ran four training courses from 2011 - 2014 and trained 27 churches and 62 teachers. Parents and teachers indicate that the children love the programme and can't wait to see what Jerry will share with them that particular week.</p>
	<img src="images/testimonials-partners01.png" width="300" height="210" style="float: right; margin: 0 0 15px 5px;" />
	<p>Teachers are excited to see how well the ten life skill lessons are linked with a Bible story. Rev Rachel and her team together with Think Twice carefully choose the appropriate Biblical stories to relate with the lesson taught. Careful thinking and input was applied to ensure that the Bible story and social issues were linked to impact the appropriate age group.</p>
	<p>Each parish receives a resource pack that consists of a variety of visuals that will help the children to enjoy the lesson and teach them valuable lessons in life. There are other useful tools such as the "talking flower " which helps to create discipline and respect inside and outside of the Sunday school class.  I will recommend this programme whole heartedly.</p>
	<p><em>- Mildred Jutzen </em></p>

    <div class="dash"></div>

    <h2>CONNECT NETWORK</h2>
	<img src="images/logo-Connect.png" width="220" height="88" style="float: right; margin: 0 0 15px 10px;" />
	<p>This testimonial serves to confirm that Think Twice is a loyal and committed member organisation of Connect Network. Connect Network is a non-profit driven collaborative entity that consist of 115 non-governmental organisations serving approximately 295,000 women and children at risk. Together, we are making a significant impact on social development in our local communities, especially in our historically disadvantaged communities within the Western Cape. </p>
	<p>ThinkTwice has been a member of the network since 2005. They are involved in and are passionate about the development of prevention and educational programmes for Foundation Phase learners (Grade R to Grade 3). </p>
	<img src="images/testimonials-partners02.png" width="300" height="210" style="float: left; margin: 0 15px 5px 0;" />
    <p>They have also demonstrated a commitment towards applying sound, ethical governance practices within their organisation as they have enrolled themselves on our international VIVA accredited Quality Improvement System (QIS). This modular QIS programme equips, helps and mentors non-governmental organisations with running and managing their organisations in a more effective, transparent, ethical and efficient manner. </p>
    <p>We are proud to recommend and endorse ThinkTwice as a potential partnering organisation. They are fulfilling a vital function in the lives of children in Cape Town and I have no hesitation in recommending them to you. </p>
	<p><em>Joseph W.A. Jacobs <br />
		Former Executive Director</em></p>


</div>
<?php include_once('partials/footer.php'); ?>
</body>
</html>
